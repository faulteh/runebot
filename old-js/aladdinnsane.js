const redis = require("async-redis");
const rClient = redis.createClient("redis://redis:6379/0");

rClient.on("error", function (err) {
    console.log("Error " + err);
});

popRedisQueue = async (rkey, nbrSamples=1) => {
    let exists = await rClient.exists(rkey);
    let qlen = await rClient.llen(rkey);
    let returnSamples = [];
    const name = rkey.charAt(0).toUpperCase() + rkey.slice(1);
    if (exists === 0 || qlen === 0) {
        returnSamples.push(`${name} is sleeping...`);
    } else {
        for (let i = 0; i < nbrSamples; i++) {
            const sample = await rClient.rpop(rkey);
            returnSamples.push(sample);
            qlen = await rClient.llen(rkey);
            if (qlen === 0) { break; }
        }
    }
    return returnSamples;
};

exports.processBowieMatrix = async (args, client, roomId, event) => {
    const body = await getBowieResponse();
    client.sendMessage(roomId, {
        msgtype: "m.notice",
        body: body
    });
};

exports.processPoeMatrix = async (args, client, roomId, event) => {
    const body = await getPoeResponse();
    client.sendMessage(roomId, {
        msgtype: "m.notice",
        body: body
    });
};

exports.getBowieResponse = async () => {
    const response = await popRedisQueue("bowie", 5);
    return response.join("\n");
};

exports.getPoeResponse = async () => {
    const response = await popRedisQueue("poe", 1);
    return response.join("");
};

exports.getLovecraftResponse = async () => {
    const response = await popRedisQueue("lovecraft", 1);
    return response.join("");
};

exports.aladdiNNsaneHelp = "AladdiNNsane:\n" +
    "  +bowie - ask Bowie to sing\n" +
    "  +poe - ask Poe to tell a story\n" +
    "  +lovecraft - ask Lovecraft to tell a story\n";
