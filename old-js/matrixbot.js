/**
 * matrixbot.js - Matrix version of the bot
 *
 * you need to have a config-matrix.json file with the following data:
 *
 * {
 *  "homeserverUrl": "https://matrix.org",
 *  "token": "auth token",
 *  "listen_channels": ["#taslug:matrix.org"],
 *  "cmd_prefix": "+",
 *  "owner_id": "your_username"
 * }
 */

const { MatrixClient, SimpleFsStorageProvider, AutojoinRoomsMixin, RichReply } = require("matrix-bot-sdk");
const config = require("./config-matrix.json");
const EightBall = require("./eightball");
const ShitTalk = require("./joker");
const AladdiNNsane = require("./aladdinnsane.js");
const Runes = require('./runes');
const Tarot = require('./tarot');

const storage = new SimpleFsStorageProvider("matrixbot.json");

const client = new MatrixClient(config.homeserverUrl, config.token, storage);
AutojoinRoomsMixin.setupOnClient(client);

client.on("room.message", async (roomId, event) => {
    if (!event["content"]) {
        // no events without content
        return;
    }
    
    if (event["content"]["msgtype"] != "m.text") {
        // only interested in text events
        return;
    }
    
    if (event["sender"] === await client.getUserId()) {
        // not interested in our own events
        return;
    }

    
    const body = event["content"]["body"];
    
    // Is it the prefix?
    if (body.indexOf(config.cmd_prefix) !== 0) {
        return;
    }
    const args = body.slice(config.cmd_prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    
    if (command === "8ball") {
        await EightBall.processCommandMatrix(args, client, roomId, event);
    }

    if (command === "tarot") {
        await Tarot.processCommandMatrix(args, client, roomId, event);
    }

    if (command === "rune") {
        await Runes.processCommandMatrix(args, client, roomId, event);
    }

    if (command === "talk") {
        await client.sendMessage(roomId, {
            "msgtype": "m.notice",
            "body": ShitTalk.processShitTalk(args.join(" "))
        });
    }

    if (command === "bowie") {
        const body = await AladdiNNsane.getBowieResponse();
        await client.sendMessage(roomId, {
            "msgtype": "m.notice",
            "body": body
        });
    }

    if (command === "poe") {
        const body = await AladdiNNsane.getPoeResponse();
        await client.sendMessage(roomId, {
            "msgtype": "m.notice",
            "body": body
        });
    }
    
    if (command === "help") {
        const help = `${Runes.runeHelp}\n${Tarot.tarotHelp}\n${EightBall.eightballHelp}\n${AladdiNNsane.aladdiNNsaneHelp}\n${ShitTalk.jokerHelp}`;
        await client.sendMessage(roomId, {
            "msgtype": "m.notice",
            "body": help
        });
    }
});

client.start().then(() => {
    console.log("Bot online");
});
