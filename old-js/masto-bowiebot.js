/**
 * masto-poebot.js - Discord auto-poster bot for Poe NN generations
 *
 * you need to have a config-mm.json file with the following data:
 *
 * {
 * "token": "PERSONALACCESSTOKEN",
 * "listen_channels": ["town-square"],
 * "cmd_prefix": "+",
 * "server": "SERVERHOST",
 * "myusername": "@botname"
 * }
 */

const config = require("./config-masto-bowiebot.json");
const AladdiNNsane = require("./aladdinnsane.js");
const Masto = require('mastodon');

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

(async () => {
    try {
        const M = new Masto({
            access_token: config.mastodon.access_token,
            api_url: config.mastodon.url
        });
        while (true) {
            let newSample = await AladdiNNsane.getBowieResponse();
            if (newSample.charAt(0) === '"') {
                newSample = newSample.slice(1);
            }
            await M.post('statuses', {status: newSample});
            console.log(`New post: ${newSample}`);
            await sleep(config.posting_interval_ms);
        }
    } catch (e) {
        console.warn(e);
    }
})();
