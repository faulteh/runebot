const EightBallData = require('./eightballdata.json');

exports.process8BallCommandDiscord = async (args, message) => {
    await message.channel.send(EightBallData[Math.floor(Math.random() * EightBallData.length)]);
};

exports.processCommandMatrix = async (args, client, roomId, event) => {
    client.sendMessage(roomId, {
        msgtype: "m.notice",
        body: EightBallData[Math.floor(Math.random() * EightBallData.length)]
    });
};

exports.get8BallResponse = () => {
    return EightBallData[Math.floor(Math.random() * EightBallData.length)];
};

exports.eightballHelp = "Magic 8-Ball:\n" +
    "  +8ball - shake the magic 8-ball\n";
