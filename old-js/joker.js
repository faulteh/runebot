/**
    joker.js - Does sentiment analysis and responds with insulting or complimentary shittalk
 */
const SW = require('stopword');
const SpellCorrector = require('spelling-corrector');
const { WordTokenizer, SentimentAnalyzer, PorterStemmer} = require('natural');
const aposToLexForm = require('apos-to-lex-form');
const spellCorrector = new SpellCorrector();

// Some node insult/compliment randomizer. Could do better
const InsultCompliment = require("insult-compliment");

spellCorrector.loadDictionary();

function analyzeSentiment(body) {
    // remove contractions, etc
    const lexed = aposToLexForm(body);
    // all letters are equal
    const cased = lexed.toLowerCase();
    // only want words
    const alphaOnly = cased.replace(/[^a-zA-Z\s]+/g, '');
    // split into words
    const tokenizer = new WordTokenizer();
    const tokenized = tokenizer.tokenize(alphaOnly);
    
    // autocorrect
    tokenized.forEach((word, index) => {
        tokenized[index] = spellCorrector.correct(word);
    });
    // remove stop words, eg but, a etc
    const filtered = SW.removeStopwords(tokenized);

    // sentiment analysis
    const analyzer = new SentimentAnalyzer('English', PorterStemmer, 'afinn');
    const analysis = analyzer.getSentiment(filtered);
    return analysis;
}

exports.processShitTalk = (body) => {
    const sentiment = analyzeSentiment(body);
    if (sentiment < 0) {
        // negative sentiment, send an insult
        return InsultCompliment.Insult();
    } else if (sentiment >= 0) {
        // positive sentiment, send a compliment
        return InsultCompliment.Compliment();
    } else {
        return 'Insert vague meh statement here';
    }
}

exports.jokerHelp = "Joker:\n" +
    "  +talk Talk to the joker\n";
