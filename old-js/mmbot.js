/**
 * mmbot.js - Mattermost version of the bot
 *
 * you need to have a config-mm.json file with the following data:
 *
 * {
 * "token": "PERSONALACCESSTOKEN",
 * "listen_channels": ["town-square"],
 * "cmd_prefix": "+",
 * "server": "SERVERHOST",
 * "myusername": "@botname"
 * }
 */

const WebSocket = require("ws");
const axios = require("axios");
const config = require("./config-mm.json");
const EightBall = require("./eightball");
const ShitTalk = require("./joker");
const AladdiNNsane = require("./aladdinnsane.js");

const ws = new WebSocket(`wss://${config.server}/api/v4/websocket`);

ws.on("open", () => {
    const msg = {
        seq: 1,
        action: "authentication_challenge",
        data: {
            token: config.token
        }
    };
    ws.send(JSON.stringify(msg));
    console.log("Bot online");
});

function sendTextReply(textResponse, endpoint, token, channelId) {
    axios({
        method: "post",
        url: endpoint,
        headers: {'Authorization': `Bearer ${token}`},
        data: {
            channel_id: channelId,
            message: textResponse
        }
    }).then((response) => { });
}

ws.onmessage = async (event) => {
    const obj = JSON.parse(event.data);
    if (obj.event === "posted") {
        const channelName = obj.data.channel_name;
        const sender = obj.data.sender_name;
        const post = JSON.parse(obj.data.post);
        const message = post.message;
        const channelId = obj.broadcast.channel_id;

        // Listening in the right channel?
        if (config.listen_channels.indexOf(channelName) === -1) { return; }

        // Don't listen to yourself
        if (sender === config.myusername) { return; }

        // Is it the prefix?
        if (message.indexOf(config.cmd_prefix) !== 0) { return; }

        const args = message.slice(config.cmd_prefix.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();

        if (command === "8ball") {
            const reply = EightBall.get8BallResponse();
            console.log(`${sender} called 8-ball, replied with ${reply}`);
            sendTextReply(reply, `https://${config.server}/api/v4/posts`, config.token, channelId);
        }

        if (command === "talk") {
            const reply = ShitTalk.processShitTalk(args.join(" "));
            console.log(`${sender} called joker, replied with ${reply}`);
            sendTextReply(reply, `https://${config.server}/api/v4/posts`, config.token, channelId);
        }

        if (command === "bowie") {
            const reply = await AladdiNNsane.getBowieResponse();
            console.log(`${sender} called bowie, replied with ${reply}`);
            sendTextReply(reply, `https://${config.server}/api/v4/posts`, config.token, channelId);
        }

        if (command === "poe") {
            const reply = await AladdiNNsane.getPoeResponse();
            console.log(`${sender} called poe, replied with ${reply}`);
            sendTextReply(reply, `https://${config.server}/api/v4/posts`, config.token, channelId);
        }

        if (command === "help") {
            const reply = "Commands:\n" + EightBall.eightballHelp;
            sendTextReply(reply, `https://${config.server}/api/v4/posts`, config.token, channelId);
        }
    }
};
