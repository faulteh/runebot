/**
 *  discordbot.js - Discord version of the bot
 *
 *  You need a config-discord.json file with contents:
 * {
 *  "token": "YOUR BOT TOKEN",
 *  "listen_channels": ["tarot", "general"],
 *  "cmd_prefix": "+",
 *  "owner_id": "ardenn#2799"
 * }
 */
const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config-discord.json');
const { processRuneCommand, runeHelp } = require('./runes.js');
const { processTarotCommand, tarotHelp } = require('./tarot.js');
const { get8BallResponse, eightballHelp } = require('./eightball.js');
const { processShitTalk } = require('./joker.js');
const { getBowieResponse, getPoeResponse, getLovecraftResponse, aladdiNNsaneHelp } = require('./aladdinnsane.js');

client.on("ready", () => {
    console.log(`Bot is online`);
    client.user.setActivity(`Efforting... Try ${config.cmd_prefix}help`);
});

client.on("message", async message => {
    // Bots don't talk to each other
    if (message.author.bot) {
        return;
    }

    // Is it the prefix?
    if (message.content.indexOf(config.cmd_prefix) !== 0) {
        return;
    }

    const channel = await message.channel.fetch();
    if (config.listen_channels.indexOf(channel.name) === -1) {
        return;
    }

    const args = message.content.slice(config.cmd_prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if (command === "rune") {
        await processRuneCommand(args, message);
    }

    if (command === "tarot") {
        await processTarotCommand(args, message);
    }

    if (command === '8ball') {
        await message.channel.send(get8BallResponse());
    }

    if (command === "talk") {
        await message.channel.send(processShitTalk(args.join(" ")));
    }

    if (command === "bowie") {
        await message.channel.send(await getBowieResponse());
    }
    
    if (command === "poe") {
        await message.channel.send(await getPoeResponse());
    }
    
    if (command === "lovecraft") {
        await message.channel.send(await getLovecraftResponse());
    }

    if (command === "help") {
        await message.channel.send(runeHelp + tarotHelp + eightballHelp + aladdiNNsaneHelp);
    }
});

client.login(config.token);
