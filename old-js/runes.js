const { MessageAttachment } = require('discord.js');
const { createCanvas, loadImage } = require('canvas');
const fs = require('fs');

const RuneData = require('./runedata.json');
const deck = RuneData.ElderFuthark.deck;
const deckName = RuneData.ElderFuthark.name;
const deckDescription = RuneData.ElderFuthark.description;

async function drawThree() {
    // Draw the runes
    const first = drawRune();
    const second = drawRune([first.rune]);
    const third = drawRune([first.rune, second.rune]);

    // Make the canvas
    const canvas = createCanvas(280, 130);
    const ctx = canvas.getContext('2d');

    // Load the images
    const firstImage = await loadImage(`./assets/runes/${first.reversed ? first.rune.image_reversed : first.rune.image}`);
    const secondImage = await loadImage(`./assets/runes/${second.reversed ? second.rune.image_reversed : second.rune.image}`);
    const thirdImage = await loadImage(`./assets/runes/${third.reversed ? third.rune.image_reversed : third.rune.image}`);

    // Make the message
    const firstString = `${first.rune.name} ${first.reversed ? '(Reversed) ' : ''}- ${first.reversed ? first.rune.description_reversed : first.rune.description}`;
    const secondString = `${second.rune.name} ${second.reversed ? '(Reversed) ' : ''}- ${second.reversed ? second.rune.description_reversed : second.rune.description}`;
    const thirdString = `${third.rune.name} ${third.reversed ? '(Reversed) ' : ''}- ${third.reversed ? third.rune.description_reversed : third.rune.description}`;
    const resultMessage = `Forecast:\nSituation: ${firstString}\nAction: ${secondString}\nOutcome: ${thirdString}`;

    // Draw the layout
    ctx.drawImage(firstImage, 20, 30, 50, 50);
    ctx.drawImage(secondImage, 100, 30, 50, 50);
    ctx.drawImage(thirdImage, 180, 30, 50, 50);
    ctx.font = "14px Impact";
    // Draw the titles of the draw on the top
    ctx.fillText("Situation", 20, 20);
    ctx.fillText("Action", 100, 20);
    ctx.fillText("Outcome", 180, 20);
    // Draw the name of the rune on bottom
    ctx.fillStyle = "#ffffff"
    ctx.fillText(first.rune.name, 20, 95);
    ctx.fillText(second.rune.name, 100, 95);
    ctx.fillText(third.rune.name, 180, 95);

    return {
        'message': resultMessage,
        'filename': 'thedraw.png',
        'buf': canvas.toBuffer()
    };
}

function drawRune(unavailble=[]) {
    // already drawn some, so only pick from the remaining deck
    const available = deck.filter((elem) => {
        return unavailble.indexOf(elem) === -1;
    });
    const pickedIndex = Math.floor(Math.random() * available.length)
    return {
        rune: available[pickedIndex],
        reversed: available[pickedIndex].reversible && Math.random() >= 0.5
    };
}

async function matrixDraw(client, roomId, event, arcana=false) {
    const draw = drawRune();
    const msg = `${draw.rune.name} ${draw.reversed ? '(Reversed)' : ''} - ${draw.reversed ? draw.rune.description_reversed : draw.rune.description}`;
    const imgData = fs.readFileSync(`./assets/runes/${draw.reversed ? draw.rune.image_reversed : draw.rune.image}`);
    const imgUrl = await client.uploadContent(imgData, 'image/png', draw.reversed ? draw.rune.image_reversed: draw.rune.image);
    await client.sendMessage(roomId, {
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
        "body": msg,
        "formatted_body": `${msg}<br><img src=${imgUrl}>`
    });
}

async function matrixDrawThree(client, roomId, event) {
    const result = await drawThree();
    const imgUrl = await client.uploadContent(result.buf, 'image/png', result.filename);
    await client.sendMessage(roomId, {
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
        "body": result.message,
        "formatted_body": `${result.message.replace(/\n/g, '<br>')}<br><img src=${imgUrl}>`
    });
}

exports.processRuneCommand = async (args, message) => {
    const command = args.shift().toLowerCase();

    if (command === "draw1") {
        const draw = drawRune();
        if (draw.reversed) {
            const attachment = new MessageAttachment(`./assets/runes/${draw.rune.image_reversed}`);
            await message.channel.send(`${draw.rune.name} (Reversed) - ${draw.rune.description_reversed}`, attachment);
        } else {
            const attachment = new MessageAttachment(`./assets/runes/${draw.rune.image}`);
            await message.channel.send(`${draw.rune.name} - ${draw.rune.description}`, attachment);
        }
    } else if (command === 'draw3') {
        const result = await drawThree(message);
        const attachment = new MessageAttachment(result.buf, result.filename);
        await message.channel.send(result.message, attachment);
    } else if (command === 'set') {
        await message.channel.send(`${deckName} - ${deckDescription}`)
    }
};

exports.processCommandMatrix = async (args, client, roomId, event) => {
    const command = args.shift().toLowerCase();
    if (command === 'draw1') {
        await matrixDraw(client, roomId, event);
    } else if (command === 'set') {
        await client.sendMessage(roomId, {
            "msgtype": "m.text",
            "body": `${deckName} - ${deckDescription}`,
        });
    } else if (command === 'draw3') {
        await matrixDrawThree(client, roomId, event);
    }
};

exports.runeHelp = "Rune Casting Commands:\n" +
    "  +rune set - Information about the current rune set\n" +
    "  +rune draw1 - Draw a single rune\n" +
    "  +rune draw3 - Draw a 3 rune forecast\n";
