#!/bin/bash

set -e

if [ "$1" = "runebot" ]; then
	cd /app
	node main.js
fi
exec "$@"
