const { MessageAttachment } = require('discord.js')
const { createCanvas, loadImage } = require('canvas')
const fs = require('fs')
const random = require('random-number-csprng')
const random2 = require('random')

const TarotData = require('./tarotdata.json')

var currentDeck = 'GoldenThread'
var readyDeck = [];
var arcanaDeck = [];
var usedArcanaDeck = [];
var usedDeck = [];

async function shuffleDeck() {
    readyDeck = [];
    usedDeck = [];
    // Make a copy of the deck
    const plainDeck = [...TarotData[currentDeck].deck];
    while (plainDeck.length > 0) {
        let idx = 0;
        if (plainDeck.length > 1) {
            idx = await random(0, plainDeck.length - 1)
        }
        if (!('description_base' in plainDeck[idx])) {
            plainDeck[idx].description_base = '';
        }
        readyDeck.push({ card: plainDeck[idx], reversed: TarotData[currentDeck].reversible ? random2.boolean() : false});
        plainDeck.splice(idx, 1);
    }
    return "Deck has been shuffled"
}

async function shuffleArcanaDeck() {
    usedArcanaDeck = [];
    const plainArcanaDeck = TarotData[currentDeck].deck.filter((card) => { return card.arcana; });
    while (plainArcanaDeck.length > 0) {
        let idx = 0;
        if (plainArcanaDeck.length > 1) {
            idx = await random(0, plainArcanaDeck.length - 1);
        }
        if (!('description_base' in plainArcanaDeck[idx])) {      // Not all sets have a description_base field
            plainArcanaDeck[idx].description_base = '';
        }
        arcanaDeck.push({ card: plainArcanaDeck[idx], reversed: TarotData[currentDeck].reversible ? random2.boolean() : false});
        plainArcanaDeck.splice(idx, 1);
    }
    return "Deck has been shuffled"
}

async function drawCard(arcana=false) {
    let card;
    let shuffled = false;
    if (arcana) {
        console.log(`Drawing from arcana only`)
        if (arcanaDeck.length === 0) {
            console.log(`Arcana deck is empty, shuffling`);
            await shuffleArcanaDeck();
            shuffled = true;
        }
        card = arcanaDeck.pop();
        usedArcanaDeck.push(card);
    } else {
        if (readyDeck.length === 0) {
            console.log(`Deck is empty, shuffling`);
            await shuffleDeck();
            shuffled = true;
        }
        card = readyDeck.pop();
        usedDeck.push(card)
    }
    console.log(card);
    card.shuffled = shuffled;
    return card;
}

async function drawThree(headings=['Past', 'Present', 'Future']) {
    const shuffleResult = await shuffleDeck();
    const first = await drawCard();
    const second = await drawCard();
    const third = await drawCard();

    // Make the canvas
    const canvas = createCanvas(500, 370);
    const ctx = canvas.getContext('2d');

    // Load the images
    const firstImage = await loadImage(`./assets/tarot/${currentDeck}/${first.reversed ? 'reversed/' : ''}${first.card.image}`);
    const secondImage = await loadImage(`./assets/tarot/${currentDeck}/${second.reversed ? 'reversed/' : ''}${second.card.image}`);
    const thirdImage = await loadImage(`./assets/tarot/${currentDeck}/${third.reversed ? 'reversed/' : ''}${third.card.image}`);

    // Make the message
    const firstString = `${first.card.name} ${first.reversed ? '(Reversed) ' : ''}- ${first.card.description_base} ${first.reversed ? first.card.description_reversed : first.card.description}`;
    const secondString = `${second.card.name} ${second.reversed ? '(Reversed) ' : ''}- ${second.card.description_base} ${second.reversed ? second.card.description_reversed : second.card.description}`;
    const thirdString = `${third.card.name} ${third.reversed ? '(Reversed) ' : ''}- ${third.card.description_base} ${third.reversed ? third.card.description_reversed : third.card.description}`;
    const resultMessage = `${shuffleResult}\n${headings[0]}: ${firstString}\n${headings[1]}: ${secondString}\n${headings[2]}: ${thirdString}`;

    // Draw the layout
    ctx.drawImage(firstImage, 10, 30, 150, 300);
    ctx.drawImage(secondImage, 170, 30, 150, 300);
    ctx.drawImage(thirdImage, 330, 30, 150, 300);
    ctx.font = "14px Impact";
    // Draw the titles of the draw on the top
    ctx.fillText(headings[0], 10, 20);
    ctx.fillText(headings[1], 170, 20);
    ctx.fillText(headings[2], 330, 20);
    // Draw the name of the rune on bottom
    ctx.fillStyle = "#ffffff"
    ctx.fillText(first.card.name, 10, 350);
    ctx.fillText(second.card.name, 170, 350);
    ctx.fillText(third.card.name, 330, 350);

    return {
        'message': resultMessage,
        'filename': 'thespread.png',
        'buf': canvas.toBuffer()
    };
}

async function matrixDraw(client, roomId, event, arcana=false) {
    const draw = await drawCard(arcana);
    const msg = `${draw.shuffled ? 'Shuffled deck\n' : ''}${draw.card.name} ${draw.reversed ? '(Reversed)' : ''} - ${draw.description_base} ${draw.reversed ? draw.card.description_reversed : draw.card.description}`;
    const imgData = fs.readFileSync(`./assets/tarot/${currentDeck}/${draw.reversed ? 'reversed/' : ''}${draw.card.image}`);
    const imgUrl = await client.uploadContent(imgData, 'image/png', draw.card.image);
    await client.sendMessage(roomId, {
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
        "body": msg,
        "formatted_body": `${msg}<br><img src=${imgUrl}>`
    });
}

async function matrixSpread(client, roomId, event, headings=['Past', 'Present', 'Future']) {
    const result = await drawThree(headings);
    const imgUrl = await client.uploadContent(result.buf, 'image/png', result.filename);
    await client.sendMessage(roomId, {
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
        "body": result.message,
        "formatted_body": `${result.message.replace(/\n/g, '<br>')}<br><img src=${imgUrl}>`
    });
}

async function deckSelect(args) {
    const newDeck = args.shift()
    let message = ''
    if (newDeck !== undefined && newDeck in TarotData) {
        currentDeck = newDeck
        message = `Switching to ${TarotData[newDeck].name}\n`
        message = message + await shuffleDeck()
    } else if (newDeck !== undefined && !(newDeck in TarotData)) {
        message = 'I do not have that deck.'
    } else {
        message = 'List of available decks:\n'
        for (deck in TarotData) {
            message = message + `     ${deck} ${currentDeck === deck ? '(Selected)' : ''} - ${TarotData[deck].description}\n`
        }
    }
    return message
}

exports.processTarotCommand = async (args, message) => {
    const command = args.shift().toLowerCase();

    if (command === "shuffle") {
        const result = await shuffleDeck();
        await message.channel.send(`${result}`);
    } else if (command === "draw") {
        if (readyDeck.length === 0) {
            await message.channel.send(await shuffleDeck());
        }
        const draw = await drawCard();
        if (draw.reversed) {
            const attachment = new MessageAttachment(`./assets/tarot/${currentDeck}/reversed/${draw.card.image}`);
            await message.channel.send(`${draw.card.name} (Reversed) - ${draw.card.description_base} ${draw.card.description_reversed}`, attachment);
        } else {
            const attachment = new MessageAttachment(`./assets/tarot/${currentDeck}/${draw.card.image}`);
            await message.channel.send(`${draw.card.name} - ${draw.card.description_base} ${draw.card.description}`, attachment);
        }
    } else if (command === "major") {
        const draw = await drawCard(true);
        if (draw.reversed) {
            const attachment = new MessageAttachment(`./assets/tarot/${currentDeck}/reversed/${draw.card.image}`);
            await message.channel.send(`${draw.card.name} (Reversed) - ${draw.card.description_base} ${draw.card.description_reversed}`, attachment);
        } else {
            const attachment = new MessageAttachment(`./assets/tarot/${currentDeck}/${draw.card.image}`);
            await message.channel.send(`${draw.card.name} - ${draw.card.description_base} ${draw.card.description}`, attachment);
        }
    } else if (command === 'ppf') {
        const result = await drawThree()
        // Send the result message
        const attachment = new MessageAttachment(result.buf, result.filename);
        await message.channel.send(result.message, attachment);
    } else if (command === 'sao') {
        const result = await drawThree(['Situation', 'Action', 'Outcome'])
        // Send the result message
        const attachment = new MessageAttachment(result.buf, result.filename);
        await message.channel.send(result.message, attachment);
    } else if (command === 'deck') {
        const resultMessage = await deckSelect(args);
        await message.channel.send(`${resultMessage}`)
    }
};

exports.processCommandMatrix = async (args, client, roomId, event) => {
    const command = args.shift().toLowerCase();
    if (command === 'draw') {
        await matrixDraw(client, roomId, event);
    } else if (command === 'major') {
        await matrixDraw(client, roomId, event, true);
    } else if (command === 'shuffle') {
        await client.sendMessage(roomId, {
            "msgtype": "m.text",
            "body": await shuffleDeck(),
        });
    } else if (command === 'deck') {
        const resultMessage = await deckSelect(args);
        await client.sendMessage(roomId, {
            "msgtype": "m.text",
            "body": `${resultMessage}`,
        });
    } else if (command === 'ppf') {
        await matrixSpread(client, roomId, event);
    } else if (command === 'sao') {
        await matrixSpread(client, roomId, event, ['Situation', 'Action', 'Outcome']);
    }
};

exports.tarotHelp = "Tarot Deck Commands:\n" +
    "  +tarot deck - Change the deck (no input lists available decks)\n" +
    "  +tarot shuffle - Shuffle the deck\n" +
    "  +tarot draw - Draw a single card\n" +
    "  +tarot major - Draw from the major arcana only\n" +
    "  +tarot ppf - Three card spread - past/present/future\n" +
    "  +tarot sao - Three card spread - situation/action/outcome\n";
