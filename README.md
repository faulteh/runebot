# RuneBot

A Simple Rune Casting Discord Bot

It has since had a few other features added:
- rune casting
- magic 8ball
- sentiment analysis shit talker/complimenter
- connection to a queue of Davie Bowie neural network generated lyrics
- connection to a queue of Edgar Allan Poe NN generated paragraphs

as well as support for additional chat platforms:
- Discord
- Mattermost
- Matrix

## Requirements

- Node 12

## Install

npm install

# Configure

There is a config-PLATFORM.json file for each type of chat platform you want
to configure. Eg, edit config-discord.json to configure for Discord

## Run

Run one or more of these:
- node discordbot.js
- node matrixbot.js
- node mmbot.js

## Build Docker Image

docker build -t runebot .

## Run In Docker

docker-compose.yml has a configuration 
docker run -it --rm -e BOT_TOKEN="YOURTOKENHERE" runebot

## AladdiNNsane Connection

The AladdiNNsane Connection is via a linked list in redis. There should be
another docker container running aladdiNNsane-redispub.py that pumps out
more lyrics/paragraphs to redis keys.

See the project: https://git.scriptforge.org/arden/aladdinnsane
